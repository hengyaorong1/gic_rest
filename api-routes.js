let router = require('express').Router();
const https = require('https');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let beerJSON = require('./beers.json');
let { uuid } = require('uuidv4');
let apiA = 'https://api.sampleapis.com/coffee/hot';
let apiB = 'https://api.sampleapis.com/beers/ale';
let imageAPI = 'https://coffee.alexflipnote.dev/random.json';
let Employee = require('./models/employee');
let genericMethod = require('./genericMethod');
let employeeController = require('./controller/employeeController');
let cafeController = require('./controller/cafeController');
mongoose.connect('mongodb://localhost/GIC', { useNewUrlParser: true });
var db = mongoose.connection;
(!db) ? (console.log("Error Connecting to Database")) : (console.log("Database Connected Successfully"));

router.use(bodyParser.json());
router.get('/drinks', async function (req, res) {
    console.log(`Drinks API`);
    console.log(`URL Segment ${req.query.type}`);
    console.log(`URL Param: ${JSON.stringify(req.params)}`);
    let resultArray = [];
    if (req.query.type == undefined) {
        console.log(`Type is Empty`);
        let apiA_promise = getHTTPPromise(apiA);
        let apiA_response = await apiA_promise;
        let apiA_jsonArray = JSON.parse(apiA_response);
        let apiB_promise = getHTTPPromise(apiB);
        let apiB_response = await apiB_promise;
        let apiB_jsonArray = JSON.parse(apiB_response);
        let apiA_result = await jsonFormat(apiA_jsonArray, 'a');
        let apiB_result = await jsonFormat(apiB_jsonArray, 'b');
        resultArray = apiA_result.concat(apiB_result);
        console.log(`Combined Coffee + Beer Amount: ${resultArray.length}`)
        res.json(genericMethod.sortByKeyDesc(resultArray, "rating"));
    }
    else if (req.query.type !== undefined && req.query.type !== '' && req.query.type !== 'coffee' && req.query.type !== 'beers') {
        console.log(`Type !== Coffee/Beers Error`)
        res.json({
            msg: 'Type Error: Provided Value is Neither Coffee nor Beer'
        })
    }
    else if (req.query.type == 'coffee') {
        console.log(`Type == coffee`);
        let apiA_promise = getHTTPPromise(apiA);
        let apiA_response = await apiA_promise;
        let apiA_jsonArray = JSON.parse(apiA_response);
        let result = await jsonFormat(apiA_jsonArray, 'a');
        console.log(result)
        res.json(result);
    }
    else if (req.query.type == 'beers') {
        console.log(`Type == beers`);
        let apiB_promise = getHTTPPromise(apiB);
        let apiB_response = await apiB_promise;
        let apiB_jsonArray = JSON.parse(apiB_response);
        let result = await jsonFormat(apiB_jsonArray, 'b');
        console.log(result)
        res.json(result);
    }
    console.log(`End`);
});
//Get all Employees and Order By Highest Number of Days Worked
router.route('/cafe/employees')
    .get(employeeController.get);
//Create New Employee
router.route('/cafe/employee')
    .post(employeeController.post);
//Create New Cafe   
router.route('/cafe')
    .post(cafeController.post);
//Get all Cafes and Order by Highest Number of Employees
router.route('/cafes')
    .get(cafeController.getByLocation);
function getHTTPPromise(urlString) {
    return new Promise((resolve, reject) => {
        https.get(urlString, (response) => {
            let chunks_of_data = [];
            response.on('data', (fragments) => {
                chunks_of_data.push(fragments);
            });
            response.on('end', () => {
                let response_body = Buffer.concat(chunks_of_data);
                resolve(response_body.toString());
            });
            response.on('error', (error) => {
                reject(error);
            });
        });
    });
}
async function jsonFormat(jsonArray, apiType) {
    let tempArray = [];
    let validBoolean = true;
    for (const j of jsonArray) {
        let beerDescription = '';
        let coffeeImage_json = {};
        if (apiType == 'a') {
            //COFFEE IMG
            let coffeeImage_promise = getHTTPPromise(imageAPI);
            let coffeeImage_response = await coffeeImage_promise;
            coffeeImage_json = JSON.parse(coffeeImage_response)['file'];
        }
        if (apiType == 'b') {
            //BEER DESCRIPTION
            let beerKeys = Object.keys(beerJSON);
            for (var i = 0; i < beerKeys.length; i++) {
                if (j.name.indexOf(beerKeys[i]) !== -1) {
                    beerDescription = beerJSON[beerKeys[i]];
                }
            }
        }
        if (apiType == 'b' && (j.rating.average == undefined)) {
            validBoolean = false;
        }
        let tempJSON = {
            name: apiType == 'a' ? j.title : j.name,
            price: apiType == 'a' ? (Math.round(Math.random() * (20 - 8) + 8) + 0.99) : j.price,
            rating: apiType == 'a' ? (Math.round((Math.random() * (5 - 1) + 1) * 100) / 100) : (Math.round(j.rating.average * 100) / 100),
            description: apiType == 'a' ? j.description : beerDescription,
            image: apiType == 'a' ? coffeeImage_json : j.image,
            id: uuid()
        }
        let keys = Object.keys(tempJSON);
        for (let i = 0; i < keys.length; i++) {
            if (tempJSON[keys[i]] == undefined) {
                validBoolean = false;
            }
        }
        if (validBoolean) {
            tempArray.push(tempJSON);
        }
    }
    console.log(`Amount of Results: ${tempArray.length}`)
    return tempArray;
}
module.exports = router;