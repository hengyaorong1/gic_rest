# GIC_REST BootUp

npm install

nodemon index

# API Info
URL: http://localhost:8085

GET Coffee : {{URL}}/drinks?type=coffee

GET Beers: {{URL}}/drinks?type=beers

GET Invalid: {{URL}}/drinks?type=friedrice

GET With No Type Specified: {{URL}}/drinks

# Docker Test

docker build . -t <your username>/node-web-app

docker run --publish 8085:8085  hengyaorong/gic_rest

docker image save gic_rest -o gic_rest.img

# Git Stuff

git remote set-url origin git@gitlab.com:hengyaorong1/gic_rest.git

