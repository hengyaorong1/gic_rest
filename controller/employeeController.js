Employee = require('../models/employee');
Cafe = require('../models/cafe');
genericMethod = require('../genericMethod')
exports.get = function (req, res) {
    Employee.get(function (err, employees) {
        if (err) {
            res.json({
                status: "Error !",
                message: err,
            });
        }
        let tempArray = employees;
        tempArray = genericMethod.sortByKeyDesc(tempArray, "days_worked");
        console.log(tempArray)
        res.json({
            status: "Success",
            message: "Employees retrieved successfully",
            data: tempArray
        });
    });
};
exports.post = function (req, res) {
    let newEmployee = new Employee();
    newEmployee.name = req.body.name;
    newEmployee.days_worked = req.body.days_worked ? req.body.days_worked : 0;
    newEmployee.cafe = req.body.cafe;
    let tempEmployeeArray = [];
    uniqueEmployee = true;
    Employee.get(async function (err, employees) {
        if (err) {
            res.json({
                status: "Error !",
                message: err,
            });
        }
        tempEmployeeArray = employees;
        for (var i = 0; i < tempEmployeeArray.length; i++) {
            if (tempEmployeeArray[i].name == newEmployee.name) { 
                console.log(`Found: ${tempEmployeeArray[i]}`);
                uniqueEmployee = false;
            }
        }
        console.log(uniqueEmployee);
        if (uniqueEmployee) {
            newEmployee.save(async function (err) {
                if (err)
                    res.json(err);
                let tempString = newEmployee._id;
                Cafe.updateOne(
                    { name: req.body.cafe },
                    { $push: { employees: tempString } },
                    function (err, numAffected) {
                        console.log(err);
                        console.log(numAffected);
                    });
                res.json({
                    message: 'New Employee Created!',
                    data: newEmployee
                });
            });
        } else {
            res.json({
                status: "Failed",
                message: "Not An Unique Employee",
            })
        }
    });
};
