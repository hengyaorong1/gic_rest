Cafe = require('../models/cafe');
genericMethod = require('../genericMethod')
exports.get = function (req, res) {
    Cafe.get(function (err, cafes) {
        if (err) {
            res.json({
                status: "Error !",
                message: err,
            });
        }
        let tempArray = cafes;
        console.log(tempArray)
        res.json({
            status: "Success",
            message: "Cafes retrieved successfully",
            data: tempArray
        });
    });
};
exports.getByLocation = async function (req, res) {
    console.log(`URL Segment ${req.query.location}`);
    let cafeRes = await Cafe.find({ location: req.query.location });
    if(cafeRes.length>0){
        res.json({
            status: "Success",
            message: "Cafes retrieved successfully",
            data: genericMethod.sortCafeEmployeesSize(cafeRes)
        });
    }else{
        res.json({
            status: "Invalid Location",
            message: "Cafes retrieved Failed",
            data: []
        });
    }
  
};
exports.post = function (req, res) {
    let newCafe = new Cafe();
    newCafe.name = req.body.name;
    newCafe.description = req.body.description;
    newCafe.logo = req.body.logo;
    newCafe.location = req.body.location;
    console.log(newCafe);
    newCafe.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Cafe Created!',
            data: newCafe
        });
    });
};
