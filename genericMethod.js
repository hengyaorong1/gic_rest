exports.sortByKeyDesc = function (jsonArray, keyString) {
    jsonArray.sort(function (a, b) {
        return b[keyString] - a[keyString];
    });
    return jsonArray;
}
exports.sortCafeEmployeesSize = function (jsonArray) {
    jsonArray.sort(function (a, b) {
        return b['employees'].length - a['employees'].length;
    });
    return jsonArray;
}
