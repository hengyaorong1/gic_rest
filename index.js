let express = require('express')
let app = express();
let apiRoutes = require("./api-routes")
var port = process.env.PORT || 8085;
app.listen(port, function () {
     console.log("Running GIC API " + port);
});
app.use('/', apiRoutes)