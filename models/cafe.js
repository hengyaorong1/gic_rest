var mongoose = require('mongoose');
var cafeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    employees: {
        type: [],
        required: false
    },
    logo: {
        type: String,
        required: false
    },
    location: {
        type: String,
        required: false
    }
});
var Cafe = module.exports = mongoose.model('cafeinfo', cafeSchema);
module.exports.get = function (callback, limit) {
    Cafe.find(callback).limit(limit);
}
