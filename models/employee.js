var mongoose = require('mongoose');
var employeeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    days_worked: {
        type: String,
        required: false
    },
    cafe: {
        type: String,
        required: true
    }
});
var Employee = module.exports = mongoose.model('employeeinfo', employeeSchema);
module.exports.get = function (callback, limit) {
    Employee.find(callback).limit(limit);
}